# fizz-buzz

[Fizz buzz](https://en.wikipedia.org/wiki/Fizz_buzz) is a group word game for children to teach them about division.

## Running

```
$ python3 app/fizzbuzz.py
```

## Running Tests

```
$ pytest tests -v -s
```
